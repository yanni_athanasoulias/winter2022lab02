public class MethodsTest {
	public static void main (String[] args) {
		int x = 10;
		System.out.println(x);
		methodNoInputReturn();
		System.out.println(x);
		methodOneInputNoReturn(3);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(3 + 4);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		methodTwoInputNoReturn(x,3.5);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double sqrt = sumSquareRoot(6,3);
		System.out.println(sqrt);
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputReturn () {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn (int x) {
		System.out.println("Inside the method one imput no return" + "  " + x);
		
	}
	public static void methodTwoInputNoReturn (int x, double y) {
		System.out.println(x + " " + y);
	}
	public static int methodNoInputReturnInt () {
		return 6;
	}
	public static double sumSquareRoot(int x, int y) {
		int add = x + y;
		double sqt = Math.sqrt(add);
		return sqt;
	}
}