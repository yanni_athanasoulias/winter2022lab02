public class AreaComputations {
	public static int areaSquare (int length) {
		return length * 4;
	}
	public static int areaRectangle (int length, int width) {
		int area = (length * 2) + (width * 2);
		return area;
	}
}